import {Input, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";

import {UserDetailInfo} from "app/shared/social/user-detail-info";
import {UserInfo} from "app/shared/auth/user-info";

import {AuthService} from "app/shared/auth/auth.service";

@Component({
    selector: 'app-display-vcard',
    templateUrl: './display-vcard.component.html',
    styleUrls: ['./display-vcard.component.css']
})
export class DisplayVcardComponent {
    // user which should be displayed
    @Input() userId: string;

    displayUser: UserDetailInfo;

    constructor(
      private authService: AuthService) {}

    ngOnInit() {
      this.authService.getUserDetail(this.userId).subscribe(userDI => {
        this.displayUser = userDI;      
      });
    }

    currentUser(): Observable<UserInfo> {
        return this.authService.currentUser();
    }


}
