import {Input, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";

import {SocialService} from "app/shared/social/social.service";

@Component({
  selector: 'following-button',
  templateUrl: './following-button.component.html',
  styleUrls: ['./following-button.component.css']
})
export class FollowingButtonComponent implements OnInit {
  // user which should be displayed
  @Input() userId: string;
  @Input() loginUserId: string;
  @Input() selfFollowing: boolean;

  fstatus: boolean;

  constructor(
    private social: SocialService) {
  }

  toggleFollow(newStatus) {
    this.social.updateFollowingInfo(this.loginUserId, this.userId, newStatus);
    this.fstatus = newStatus;
  }

  ngOnInit() {
    if (this.loginUserId == this.userId) {
      this.fstatus = this.selfFollowing;
    }
    else {
      this.social.getFollowingInfo(this.loginUserId, this.userId)
        .subscribe(info => {
          this.fstatus = info ? info.status : false;
        });
    }
  } // onInit
}
