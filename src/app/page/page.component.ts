import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from "@angular/core";
import {Observable} from "rxjs";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { AuthService } from "app/shared/auth/auth.service";

import { SnippetInfo } from "app/shared/extension/snippet-info";
import { SnippetorService } from 'app/extension/snippetor/snippetor.service';
import { UserInfo } from "app/shared/auth/user-info";
@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
    @ViewChild('container', {read: ElementRef}) container: ElementRef;
    title = '';
    alertType = null;
    private alertMessage = "";

    mode  = null;
    did = null;
    user: UserInfo;

    vcard: string;


    constructor(
        private auth: AuthService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
      this.auth.currentUser().subscribe(user => this.user = user);

        this.route.data.subscribe(data => {
          let mode = data["mode"]
          if (mode == "dashboard") {
            this.route.paramMap.subscribe(pMap => {
              let uid = pMap.get("uid");
              if (!uid) {
                this.auth.currentUser().subscribe(user => this.vcard = user.uid);
              }
              else {
                this.vcard = uid;
              }

            });
          }
          else {
            this.auth.currentUser().subscribe(user => this.vcard = user.uid);
          }
        });
    }

    isLoggedIn(): Observable<boolean> {
        return this.auth.isLoggedIn();
    }

    onResetPasswordSuccess() {
        this.alertType = "success";
        this.alertMessage = "Reset Password Sent!";
    }

    onLoginSuccess() {
        this.alertType = "success";
        this.alertMessage = "Login Success!";
    }

    onRegisterSuccess() {
        this.alertType = "success";
        this.alertMessage = "User registered!";
    }

    onError(err) {
        this.alertType = "danger";
        this.alertMessage = err;
    }

    onLoggedOut() {
        // Just reset any displayed messsage.
        this.alertType = null;
        this.alertMessage = "";
    }

    alertClosed() {
        this.alertType = null;
        this.alertMessage = "";
    }
}
