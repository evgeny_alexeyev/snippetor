import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {AuthService} from "app/shared/auth/auth.service";

import {SnippetInfo} from "app/shared/extension/snippet-info";
import {SnippetorService} from 'app/extension/snippetor/snippetor.service';
import {UserInfo} from "app/shared/auth/user-info";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit   {
    private title = '';
    private alertType = null;
    private alertMessage = "";

    mode  = null;
    did = null;
    user: UserInfo;


    constructor(private auth: AuthService
                ) {
      // There is no way to get routes here
    }

    ngOnInit() {
     // There is no way to get routes here
    }

    isLoggedIn(): Observable<boolean> {
        return this.auth.isLoggedIn();
    }

    onResetPasswordSuccess() {
        this.alertType = "success";
        this.alertMessage = "Reset Password Sent!";
    }

    onLoginSuccess() {
        this.alertType = "success";
        this.alertMessage = "Login Success!";
    }

    onRegisterSuccess() {
        this.alertType = "success";
        this.alertMessage = "User registered!";
    }

    onError(err) {
        this.alertType = "danger";
        this.alertMessage = err;
    }

    onLoggedOut() {
        // Just reset any displayed messsage.
        this.alertType = null;
        this.alertMessage = "";
    }

    alertClosed() {
        this.alertType = null;
        this.alertMessage = "";
    }
}
