import * as _ from 'lodash';
import {Component, OnInit, EventEmitter, Output} from "@angular/core";

import {UserInfo} from "app/shared/auth/user-info";
import {SnippetInfo} from "app/shared/extension/snippet-info";
import {Observable} from "rxjs";

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {SocialService} from "app/shared/social/social.service";
import {FirebaseObjectObservable } from 'angularfire2/database';

import {SnippetorService} from 'app/extension/snippetor/snippetor.service';

import {AuthService} from "app/shared/auth/auth.service";

@Component({
  selector: 'snippet-detailed-view',
  templateUrl: './snippetview.component.html',
  styleUrls: ['./snippetview.component.css']
})
export class SnippetViewComponent implements OnInit {
  mode: string;
  snippet: SnippetInfo;
  user: UserInfo;
  editable: string;
  state: string;

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        let mode = data["mode"];
        this.editable = data["mode"] != "view" ? "" : "not-editable";
        this.mode = mode;
        if (mode == "draft") {
          this.extSrv.getWorkingSnippet().subscribe((x) => {
            this.snippet = x;
            this.state = null;
          });
        }
        else {
          this.route.paramMap.subscribe(pMap => {
            let uid = pMap.get("uid");
            if (uid) {
              this
              .social
              .getSnippet(uid)
              .subscribe((snip) => {
                snip.uid = snip["$key"] || null;
                this.state = null;
                this.snippet = snip;
              });
            }
            else {
              this.state = "failed";
            }
          });
        }
      });
    /*
        this
        .route
        .paramMap
        .switchMap((params: ParamMap) => {
          if (this.mode == "draft") {
            return this.extSrv.getWorkingSnippet();
          }
          return this.social.getSnippet(params.get('uid'));
        }).subscribe((snip: SnippetInfo) => {
          if (snip) {
            snip.uid = snip["$key"] || null;
            this.state = null;
          }
          else
            this.state = "failed";


          this.snippet = snip;
        });
        */
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private social: SocialService,
    private extSrv: SnippetorService,
    private auth: AuthService
  ) {
    this.user = null;
    this.state = "loading";
    // get current user
    this.auth.currentUser().subscribe(user => {
      this.user = user;
    });
  }

  playSnippet() {
    this.extSrv.openSnippet(this.snippet);
  }

  //
  // There are a lot of buttons available
  // but not all of them should be displayed
  //
  getVisibility(button) {
    let vis = "none";
    let nouser = (this.user == null || this.user.isAnonymous);

    if (this.mode == "view") {
      if (["close", "edit", "fork", "likewatch"].indexOf(button) >= 0)
        vis = "";
    }
    else if (this.mode == "edit") {
      if (["save", "revert", "garbadge"].indexOf(button) >= 0)
        vis = "";
    }
    else if (this.mode == "draft") {
      if (["save", "delete", "garbadge"].indexOf(button) >= 0)
        vis = "";
    }

    // no way to modify snippet if user not log-inned
    if (nouser) {
      if (["save", "likewatch", "fork"].indexOf(button) >= 0)
        vis = "none";
    }
    // prevent user to like and watch themself
    else if (this.user.uid == this.snippet.user_id && button == "likewatch") {
      vis = "none";
    }
    else if (this.user.uid != this.snippet.user_id && button == "edit") {
      vis = "none";
    }


    return vis;
  }

  editSnippet() {
    // change routing only
    // the same snippet but edit mode
    this.router.navigate(["/edit", this.snippet["$key"]]);
  }

  changeSnippetField(field, value, action = 'add') {
    if (field === 'tags') {
      _.defaults(this.snippet, { 'tags': [] });
      if (action === 'add') {
        this.snippet.tags.push(value);
      } else if (action === 'remove') {
        _.remove(this.snippet.tags, (el) => el === value);
      }

      value = this.snippet.tags;
    }
    else if (field == 'description') {
      this.snippet.description = value;
    }
    else if (field == 'title') {
      this.snippet.title = value;
    }
    this.extSrv.updateSnippet(
      _.set({}, field, value)
    );
  }

  deleteSnippet() {
    // Delete called for draft ONLY
    // therefore we need to notify extension
    this.extSrv.closeSnippet(this.snippet);
    // and route to the search or user dashboard

    // Navigate to root or dashboard
    if (this.user && !this.user.isAnonymous) {
      this.router.navigate(["/dashboard"]);
    }
    else
      this.router.navigate(["/"]);
  }

  saveSnippet() {
    // 1. call firebase save
    this.snippet.items = this.snippet.items || [];
    this.social.saveSnippet(this.snippet, this.user).subscribe(success => {
      if (success)
        this.extSrv.closeSnippet(this.snippet);
      setTimeout(() => {
        this.router.navigate(["/view", this.snippet["$key"] || success]);
      }, 200);
    });
    // 2. then call snippet save notification to the extension

    // 3. trigger routing to the snippet view or user dashboard
    //    (to see new snippet in the list of snippets)
  }

  closeSnippet() {
    this.extSrv.closeSnippet(this.snippet);
    setTimeout(() => {
      this.router.navigate(["/"]);
    }, 100);
  }

  revertSnippet() {
    // 1. UI update: revert changes to the origin one (formally cancel modifications)
    // 2. extension: return an original snippet value
    // 3. change route to view/UID
    this.router.navigate(["/view", this.snippet["$key"]]);
  }

  forkSnippet() {
    // fork current snippet (available for a view mode only)
    this.social.forkSnippet(this.snippet, this.user).subscribe((path) => {
      if (path != null) {
        this.router.navigate(["/edit", path]);
      }
      else {
        // TODO: SHOW ERROR LABEL
      }

    });
    // routing goes to the edit mode for a forked snippet (no reason to fork snippet without changes)
  }

  onDeleteSnippetItem(index) {
    this.extSrv.updateSnippetItem('delete', { index: index });
  }

  onChangeShippetItem(index, comment) {
    this.extSrv.updateSnippetItem('update', {
      idx: index,
      item: { comment: comment }
    });
  }

}
