import {SnippetItemInfo} from "app/shared/extension/snippetitem-info";


export class SnippetInfo {
  // snippet unique ID
  uid: string;
  // Title and description
  title: string;
  description: string;
  // arrays of tags and branches
  tags: string[];
  branches: string[];
  // user who created snippet
  // display name and photo url

  // OVERHEAD
  user_id: string;
  user_name: string;
  photoURL?: string;

  // List of snippet items
  items: SnippetItemInfo[];

  // working states only
  state: string; // new draft/opened/modified

  // Date of creation and modification
  created: Date;
  modified: Date;

  fork: any;
  likes: Number;
  watches: Number;
}
