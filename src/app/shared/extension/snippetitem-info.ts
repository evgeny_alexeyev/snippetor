export class SnippetItemInfo {
    comment: string;
    line: number;
    path: string;
    source: string;
}
