export class UserDetailInfo {
    displayName: string; // name to display
    photoUrl : string; // user's avatar
    isSelfFollowing: boolean; // self following indicator
    following: number;  // following to
    follower: number;   // following you
}
