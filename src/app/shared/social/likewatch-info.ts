// Handle like and watch
// affects the corresponding firebase functions

export class LikeWatchInfo {
    like: boolean;
    watch: boolean;
    userId: string;
    snippetId: string;
}
