import {Injectable, Inject} from "@angular/core";

import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import {FirebaseApp} from "angularfire2";

import {SnippetInfo} from "app/shared/extension/snippet-info";
import {LikeWatchInfo} from "./likewatch-info";
import {UserDetailInfo} from "./user-detail-info";
import {FollowInfo} from "./follow-info";

import {Observable, Subject, ReplaySubject, AsyncSubject} from "rxjs";

@Injectable()
export class SocialService {
  private snippetSubject: ReplaySubject<SnippetInfo>;
  private fstatus: ReplaySubject<FollowInfo>;

  snippets: FirebaseListObservable<any[]>;
  constructor(private db: AngularFireDatabase, @Inject(FirebaseApp) firebaseApp: any) {
    this.initsnippetSubject();
  }

  private initsnippetSubject() {
    this.snippetSubject = new ReplaySubject<SnippetInfo>(1);
    this.fstatus = new ReplaySubject<FollowInfo>(1);
  }

  listSnippets(query = null): FirebaseListObservable<any[]> {
    // BUILD QUERY
    let request = {
      query: {
        limitToLast: 10
      }
    };
    if (query != null) {
      if (query.user) {
        request.query["orderByChild"] = 'user_id';
        request.query["equalTo"] = query.user;
      }

      if (query.startAt) {
        request.query["startAt"] = query.startAt;
        request.query["orederByKey"] = true;
      }

      if (query.page > 0) {
        request.query.limitToLast = query.page * 10;
      }
    }

    // send request to firebase
    return this.db.list("/messages", request).map((array) => array.reverse()) as FirebaseListObservable<any[]>;
  }

  getSnippet(uid): FirebaseObjectObservable<SnippetInfo> {
    return this.db.object('/messages/' + uid);
  }

  _cleanUpPayload(pl: SnippetInfo): SnippetInfo {
    let res: SnippetInfo = new SnippetInfo();
    let fields = ["title", "description", "tags", "items",
      "user_id", "user_name", "photoUrl",
      "created", "modified",
      "fork", "version",
      "like", "watch",
      "label"];
    for (let v in fields) {
      let k = fields[v];
      res[k] = pl[k] || "";
    }

    res.modified = new Date();
    return res;
  }

  /////////////////////////////////////////////
  //                 SNIPPET                 //
  /////////////////////////////////////////////

  saveSnippet(payload: SnippetInfo, user: any): Observable<string> {
    return new Observable(observer => {
      if (payload.uid) {
        this.db.object('/messages/' + payload.uid)
          .update(this._cleanUpPayload(payload))
          .then(data => {
            if (data) {
              let path = data["path"].toString();
              path = path.split("/");
              observer.next(path.pop());
            }
            else
              observer.next(payload.uid);
          });
      }
      else {
        let pl = {
          ////////////////////////////// snippet payload itself
          description: payload.description || "",
          title: payload.title || "",
          items: payload.items || [],
          tags: payload.tags || [],
          ////////////////////////////// Dates
          created: payload.created || new Date(),
          modified: new Date(),
          ////////////////////////////// overhead for preview of snippet
          user_id: user.uid,
          user_name: user.displayName,
          user_photo: user.photoURL,
          ////////////////////////////// overhead for future
          version: 0, //
          likes: 0,
          watches: 0,
          label: '',
          fork: payload.fork || '' // set up fork == '' to prevent redefine in the future
        };

        this.db.list("/messages").push(pl).
          then(data => {
            let path = data.path.toString();
            path = path.split("/");
            let subpath = path.pop() || path.pop();
            observer.next(subpath);
          });
      }
    });
  }

  forkSnippet(payload: SnippetInfo, user: any): Observable<string> {

    payload.fork = payload.uid;
    payload.uid = null;
    return this.saveSnippet(payload, user);
  }

  deleteSnippet(uid) {
    this.db.list("/messages").remove(uid);
  }

  currentSnippet(): Observable<SnippetInfo> {
    return this.snippetSubject.asObservable();
  }

  hasSnippet(): Observable<boolean> {
    let subj = new AsyncSubject<boolean>();
    this.snippetSubject.subscribe(snippet => {
      subj.next(snippet.uid != null);
      subj.complete();
    });
    return subj;
  }
  /////////////////////////////////////////////
  //                 FOLLOWING               //
  /////////////////////////////////////////////
  //
  // Create or update status of the following
  // one user to another one
  //
  updateFollowingInfo(uid, fuid, status) {
    let that = this;
    this.db.object('/follow/' + uid + fuid)
      .$ref.ref.transaction(ud => {
        if (ud === null) {
          ud = {
            follower: uid,
            following: fuid,
            status: status == true // true or false
          };
        }
        else {
          ud.status = status == true;
        }

        that.fstatus.next(ud);
        return ud;
      });
  }
  //
  // Get following information
  //
  getFollowingInfo(uid, fuid) {
    return this.db.object('/follow/' + uid + fuid);
  }

  /////////////////////////////////////////////
  //                 LIKE AND WATCH          //
  /////////////////////////////////////////////
  getLikeAndWatch(suid, uuid): Observable<LikeWatchInfo> {
    return this.db.object('/likewatch/' + suid + uuid);
  }

  setLikeAndWatch(info: LikeWatchInfo): Observable<boolean> {
    return new Observable(obs => {
      this.db
        .object('/likewatch/' + info.snippetId + info.userId)
        .update(info)
        .then(data => obs.next(true));
    });
  }
}
