// Handle user to user follow information
// affects the corresponding firebase functions

export class FollowInfo {
    active: boolean;
    following: string;
    follower: string;
}
