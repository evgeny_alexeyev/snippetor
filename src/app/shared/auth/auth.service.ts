import {Injectable, Inject} from "@angular/core";
import {Observable, Subject, ReplaySubject, AsyncSubject} from "rxjs";

import {AngularFireAuth} from "angularfire2/auth";
import {FirebaseApp} from "angularfire2";
import * as firebase from 'firebase/app';

import {UserInfo} from "./user-info";
import {UserDetailInfo} from "../social/user-detail-info";

import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import User = firebase.User;

@Injectable()
export class AuthService {
  private userInfoSubject: ReplaySubject<UserInfo>;
  private userDetail: ReplaySubject<UserDetailInfo>;
  private user: User;
  //private firebaseAuth: Auth;

  constructor(private db: AngularFireDatabase,
              private angularFireAuth: AngularFireAuth,
              @Inject(FirebaseApp) firebaseApp: any) {
    this.initUserInfoSubject();
    // console.log("AuthService");
    //this.firebaseAuth = firebaseApp.auth();
    let that = this;
    angularFireAuth.authState.subscribe(user_data => {
      // console.log("user: ", JSON.stringify(auth));

      let userInfo = new UserInfo();
      if (user_data != null) {
        this.user = user_data;
        userInfo.isAnonymous = user_data.isAnonymous;
        userInfo.email = user_data.email;
        userInfo.displayName = user_data.displayName;
        userInfo.providerId = user_data.providerId;
        userInfo.photoURL = user_data.photoURL;
        userInfo.uid = user_data.uid;

        that.createOrUpdateUserDetail(user_data);
      } else {
        this.user = null;
        userInfo.isAnonymous = true;
      }
      this.userInfoSubject.next(userInfo);

    });
  }

  private initUserInfoSubject() {
    this.userInfoSubject = new ReplaySubject<UserInfo>(1);
    this.userDetail = new ReplaySubject<UserDetailInfo>(1);
  }

  currentUser(): Observable<UserInfo> {
    return this.userInfoSubject.asObservable();
  }

  logout(): Observable<string> {
    let result = new Subject<string>();
    //this.initUserInfoSubject();
    this.angularFireAuth.auth.signOut()
      .then(() => result.next("success"))
      .catch(err => result.error(err));
    return result.asObservable();
  }

  // create or update user detail
  createOrUpdateUserDetail(user) {

  if (!user.uid) return;

  let that = this;
    this.db.object('/user_details/' + user.uid).$ref
      .ref.transaction(ud => {
        if (ud === null) {
          ud = {
            followers: 0,
            following: 0,
            selfFF: false,
            name: user.displayName,
            photo: user.photoURL
          };
        }

        that.userDetail.next(ud);

        return ud;
      });
  }

  getUserDetail(userId) {
    return new Observable<UserDetailInfo>(observer => {
          this.db.object('/user_details/' + userId).subscribe(details => {
            // different user requested
            observer.next(details);
          });
      });
  }

  isLoggedIn(): Observable<boolean> {
    let isLoggedInBS = new AsyncSubject<boolean>();
    this.userInfoSubject.subscribe(ui => {
      // console.log("isLoggedIn: anonymous=" + ui.isAnonymous);
      isLoggedInBS.next(!ui.isAnonymous);
      isLoggedInBS.complete();
    });
    return isLoggedInBS;
  }

  loginViaProvider(provider: string): Observable<String> {
    let result = new Subject<string>();
    if (provider === "google") {
      this.angularFireAuth.auth
        .signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then(auth => result.next("success"))
        .catch(err => result.error(err));
      return result.asObservable();
    }
    else if (provider === "github") {
      this.angularFireAuth.auth
        .signInWithPopup(new firebase.auth.GithubAuthProvider())
        .then(auth => result.next("success"))
        .catch(err => result.error(err));
      return result.asObservable();
    }
    result.error("Not a supported authentication method: " + provider)
    return result.asObservable();
  }
}
