import {Pipe, PipeTransform} from '@angular/core';
//import {invalidPipeArgumentError} from './invalid_pipe_argument_error';

@Pipe({ name: 'getbranches' })
export class GetBranchesPipe implements PipeTransform {
  transform(value: Array<any>): Array<any> {
    if (!value) return value;

    if (value.length === undefined)
      value = [value];
    for (let snip in value) {
        value[snip].items = value[snip].items || [];
        value[snip].branches = this.extractBranchesFromItems(value[snip].items);
        value[snip].n_comments = value[snip].items.length;
        value[snip].likes = "" + (value[snip].likes || 0);
        value[snip].watches = value[snip].watches || 0;
    }
    return value;
  }

  extractBranchesFromItems(items) {
    if (!items)
      return [];

    let uitems = [];
    for (let key in items) {
      let item = items[key];
      let branch = null;
      if (item.url.indexOf("https://github.com") == 0) {
        let rr = item.url.split("/");
items[key].source = "github";
        items[key].username = rr[3];
        items[key].repo = rr[4];
        items[key].path = item.url.substr(21 + rr[3].length + rr[4].length );
        branch = {source: "github", name: rr[3] + "/" + rr[4]};
      } else if (item.url.indexOf("https://bitbucket.org") == 0) {

        let rr = item.url.split("/");
items[key].source = "bitbucket";
        items[key].username = rr[3];
        items[key].repo = rr[4];
        items[key].path = item.url.substr(24 + rr[3].length + rr[4].length );

        branch = {source: "bitbucket", name: rr[3] + "/" + rr[4]};
      } else if (item.url.indexOf("https://cs.chromium.org") == 0) {
        branch = {source: "googlecode", name: "cs.chromium.org"};
items[key].source = "google";
        items[key].username = "google";
        items[key].repo = "chromium";
        items[key].path = item.url.substr(24);

      }
      // Add branch to the list
      // if it is not in the list
      if (branch != null) {
        let found = false;
        for (let b in uitems)
          if (uitems[b].source == branch.source
            && uitems[b].name == branch.name)
            found = true;
        if (found == false)
          uitems.push(branch);
      }
    }
    return uitems;
  }
}
