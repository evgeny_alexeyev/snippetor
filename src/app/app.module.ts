import {BrowserModule} from "@angular/platform-browser";

import {NgModule} from "@angular/core";

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {firebaseConfig} from "environments/firebaseConfig";
import {AngularFireModule} from "angularfire2";
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { VirtualScrollModule } from 'angular2-virtual-scroll';
import { FlexLayoutModule } from '@angular/flex-layout';

import {AuthService} from "app/shared/auth/auth.service";

import {AppComponent} from "./app.component";
import {DisplayUserComponent} from "app/display-user/display-user.component";
import {DisplayInstallComponent} from "app/display-install/display-install.component";
import {DisplayVcardComponent} from "app/display-vcard/display-vcard.component";
import {FollowingButtonComponent} from "app/display-vcard/following-button/following-button.component";
import {SearchResultComponent} from "app/search-result/search-result.component";
import {SnippetViewComponent} from "app/snippetview/snippetview.component";

import {PageComponent} from 'app/page/page.component';

import {AlertModule} from "ngx-bootstrap";

import {NavigationComponent} from './navigation/navigation.component';
import {SearchComponent} from './search/search.component';
import {TabsComponent} from './tabs/tabs.component';

import { BsDropdownModule } from 'ngx-bootstrap';

import {SocialService} from "app/shared/social/social.service";
import {SnippetorService} from 'app/extension/snippetor/snippetor.service';

import {GetBranchesPipe} from "app/shared/getbranches.pipe";


import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'view/:uid', component: PageComponent,
     children: [
       {path:'', component: SnippetViewComponent, outlet:'content', data: {mode: "view"}}
     ],
     data: {mode: "view"}
  },
  { path: 'edit/draft', component: PageComponent,
     children: [
       {path:'', component: SnippetViewComponent, outlet:'content', data: {mode: "draft"}}
     ],
     data: {mode: "draft"}
  },
  { path: 'edit/:uid', component: PageComponent,
     children: [
       {path:'', component: SnippetViewComponent, outlet:'content', data: {mode: "edit"}}
     ],
     data: {mode: "edit"}
  },
  { path: 'dashboard/:uid', component: PageComponent,
     children: [
       {path:'', component: SearchResultComponent,  outlet:'content', data: {mode: "dashboard"}}
     ],
     data: {mode: "dashboard"}
  },
  { path: 'dashboard', component: PageComponent,
     children: [
       {path:'', component: SearchResultComponent, outlet:'content', data: {mode: "dashboard"}}
     ],
     data: {mode: "dashboard"}
  },
  { path: 'search', component: PageComponent,
     children: [
       {path:'', component: SearchResultComponent, outlet:'content', data: {mode: "search"}}
     ]
  },
  { path: '', component: PageComponent,
     children: [
       {path:'', component: SearchResultComponent, outlet:'content', data: {mode: "index"}}
     ]
  }
  // TBD PAGE NOT FOUND component
];

@NgModule({
    declarations: [
        AppComponent,
        DisplayInstallComponent,
        DisplayUserComponent,
        DisplayVcardComponent,
        FollowingButtonComponent,
        NavigationComponent,
        SearchComponent,
        SearchResultComponent,
        SnippetViewComponent,
        TabsComponent,
        GetBranchesPipe,
        PageComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AlertModule.forRoot(),
        BsDropdownModule.forRoot(),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        TagInputModule,
        BrowserAnimationsModule,
        VirtualScrollModule,
        FlexLayoutModule,
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: true } // <-- debugging purposes only
        )
    ],
    providers: [AuthService, SocialService, SnippetorService ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
