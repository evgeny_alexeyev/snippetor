import {Component, OnInit} from "@angular/core";
import {
  FormControl,
  FormGroup
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'navbar-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent {
   searchControl = new FormControl();
   search = null;

   query: string;
   user: string;
   branch: string;
   tags: string[];

   constructor(private router: Router,
   public activated: ActivatedRoute) {
     this.query = '';
   }

   ngOnInit() {
    this.searchControl.valueChanges.subscribe(value => {
      this.search = value;
    });

    this.activated.params.subscribe(
      params => {
        this.query = params['query'];
        this.user = params['user'];
        //this.tags
        this.branch = params['branch'];
      }
    );
  }

  simpleSearch() {
    if (!this.search || this.search == "")
      return;
    this.router.navigate(["/search", {query: this.search}]);
  }
}
