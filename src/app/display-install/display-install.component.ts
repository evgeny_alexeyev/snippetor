import {Input, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";

import {SnippetorService} from 'app/extension/snippetor/snippetor.service';

@Component({
    selector: 'app-display-install',
    templateUrl: './display-install.component.html',
    styleUrls: ['./display-install.component.css']
})
export class DisplayInstallComponent {
    needInstall: Boolean;

    constructor(    private extSrv: SnippetorService) {
      this.needInstall = null;
    }

    ngOnInit() {
      this.extSrv.isPluginInstalled().subscribe(status => {
        this.needInstall = status != true;
      });
    }
}
