import * as _ from 'lodash';
import {Injectable, Inject} from "@angular/core";

import {SnippetInfo} from "app/shared/extension/snippet-info";

import {Observable, ReplaySubject, BehaviorSubject} from "rxjs";

/*
//
// Communication with an extension web-site injection plugin
//
var snippetorExtensionService = {
  observers: [],
  addObserver: function(observer) {
    this.observers.push(observer);
  },
  openSnippet: function(payload, idx) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "open-snippet",
        data: {
          payload: payload,
          index: idx
        }
      }
    }));
  },
  onSavedSnippet: function(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "saved-draft",
        payload: payload
      }
    }));
  },
  onEditCurrentSnippet: function(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "edit-current-snippet",
        payload: payload
      }
    }));
  },
  subscribers: [],
  onUnsubscribeSnippet: function(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "unsubscribe",
        payload: payload
      }
    }));
  },
  //
  // HAndle snippet items
  //
  onAddItem: function(payload) {
    if (!this.initialized)
      return;
    // Update current cache (TBD: fo future when it will be not necessary to reload all page)
    this.snippets[payload.working].items.splice(payload.index, 0, payload.item);
    //
    // Notify only if we are subscribed on it
    if (payload.working == this.working_snippet) {
      // Handle UI part
      this.notifyObservers("onAddItem", payload);
    }
  },
  onRemoveItem: function(payload) {
    if (!this.initialized)
      return;
    this.snippets[payload.working].items.splice(payload.index, 1);
    // Handle UI part
    if (payload.working == this.working) {
      this.notifyObservers("onRemoveItem", payload);
    }
  },
  onUpdateItem: function(payload) {
    if (!this.initialized)
      return;
   this.snippets[payload.working].items[payload.payload.idx].comment = payload.payload.item.comment;
    // Handle UI part
    if (payload.working == this.working) {
      this.notifyObservers("onUpdateItem", payload.payload);
    }
  },
  onMoveItem: function(payload) {
    if (!this.initialized)
      return;
    var item = this.snippets[payload.working].items[payload.payload.oldIndex];
    this.snippets[payload.working].items.splice(payload.payload.oldIndex, 1);
    this.snippets[payload.working].items.splice(payload.payload.newIndex, 0, item);
    // Handle UI part
    if (payload.working == this.working) {
      this.notifyObservers("onMoveItem", payload.payload);
    }
  },
  //
  // Notify all observers whic has "method"
  //
  notifyObservers: function(method, payload) {
    for (var x = 0; x < this.observers.length; ++x) {
      if (this.observers[x] && this.observers[x][method]) {
        this.observers[x] && this.observers[x][method](payload);
      }
    }
  },
  snippets: [],
  working_snippet: -1,
  initialized: null,

  isInitialized: function() {
    return this.initialized;
  },
  //
  // Indicates is extension was installed
  //
  hasExtension: function() {
    return true;
  },
  //
  // Get current working snippet
  //
  getWorkingSnippet2: function() {
    if (this.initialized && this.working_snippet >= 0 && this.snippets.length > 0) {
      return this.snippets[this.working_snippet];
    }
    return null;
  },
  //
  // Get opened snippet by UID
  //
  getSnippetByUid: function(uid) {
    if (!this.initialized)
      return;
    for (var r in this.snippets) {
      if (this.snippets[r] && this.snippets[r].uid == uid)
        return this.snippets[r];
    }
    return null;
  },
  //
  // subscribed for callbacks
  //
  init: function() {
    var that = this;

    //////////////////////////////////////////////////////////////
    // we need to request GetInitialItems, on start up to get
    // callback this event
    //
    //////////////////////////////////////////////////////////////
    window.addEventListener("onInit", function(evt) {
      that.initialized = true;
      that.isInstalled.next(true);
      that.snippets = evt.detail.snippets;
      that.working_snippet = evt.detail.working;
      snippetorExtensionService.notifyObservers("initCallback", evt.detail);
    });

    //////////////////////////////////////////////////////////////
    // triggers snippet change events:
    // save - snippet was saved and closed
    // create - a new snippet was created
    // open   - snippet was opened via web interface from the firebase storage
    // edit-state - snippet was switched to EDIT mode (not working yet)
    //////////////////////////////////////////////////////////////
    window.addEventListener("onSnippetChange", function(evt) {
      var payload = evt.detail;
      if (payload.action == "save") {
        // SAVED SNIPPET NOT LONGER AVAILABLE
        that.snippets[payload.working] = null;
        snippetorExtensionService.notifyObservers("onSaveSnippet", payload);
      } else if (payload.action == "create") {
        // CREATE DRAFT FROM SCRATCH
        that.snippets[payload.working] = payload.snippets;
        snippetorExtensionService.notifyObservers("onCreateSnippet", payload);
      } else if (payload.action == "open") {
        // OPEN SNIPPET FROM THE WEB
        that.snippets[payload.working] = payload.snippets;
      snippetorExtensionService.notifyObservers("onOpenSnippet", payload);
      } else if (payload.action == "edit-state") {
        console.log("TODO: ADD edit state handling");
      } else {
        alert("Unknow snippet action: " + payload.action);
      }
    });

    //////////////////////////////////////////////////////////////
    // Each snippet consists of comments == item
    // items could be changed:
    // add - add new comment
    // remove - comment was removed
    // update - comment's text was modified
    // move - index/position of the comment was changes (sortable comments)
    //////////////////////////////////////////////////////////////
    window.addEventListener("onSnippetItemChange", function(evt) {
      var payload = evt.detail;
      if (payload.action == "add") {
        that.onAddItem(payload);
      } else if (payload.action == "remove") {
        that.onRemoveItem(payload);
      } else if (payload.action == "update") {
        that.onUpdateItem(payload);
      } else if (payload.action == "move") {
        that.onMoveItem(payload);
      } else {
        alert("Unknow snippet action: " + payload.action);
      }
    });

    //////////////////////////////////////////////////////////////
    // request all snippets from the extension on start-up
    //////////////////////////////////////////////////////////////
    setTimeout(function() {
      console.log("REQUEST INITIAL STATE!!!!");
      window.dispatchEvent(new CustomEvent("onSnipettorAction", {
        detail: {
          action: "GetInitialState"
        }
      }));
    }, 100);
  },
  //
  // Assign current tab to the concreate snippet or snippet draft
  // @param payload - index of the selected item, (-1) is to unsubscribe
  //
  selectSnippetByIndex: function(index) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "select-snippet",
        data: index
      }
    }));
  },
  //
  // Update current snippet data:
  // title, hash-tags or description
  //
  updateSnippet: function(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "update-snippet",
        payload: payload
      }
    }));
  },
  //
  // Update the concreate snippet item's description
  // 1. update - update description
  // 2. delete - item removed
  // 3. position - change item position
  //
  updateSnippetItem: function(action, payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: action + "-snippet-item",
        payload: payload
      }
    }));
  }
};
*/

@Injectable()
export class SnippetorService {
  private snippetsSubject: BehaviorSubject<SnippetInfo[]>;
  private workingSnippet: ReplaySubject<SnippetInfo>;
  private isInstalled: ReplaySubject<Boolean>;

  private working_snippet: number;
  private initialized : boolean;

  constructor() {
    // initialize all subjects
    this.initsnippetSubject();

    // ???
    this.workingSnippet = new ReplaySubject<SnippetInfo>(1);
    // suppose that plugin not available yet
    this.initialized = false;

    window.addEventListener("onInit", (evt: CustomEvent) => {
      // Handle install plugin message
      this.initialized = true;
      this.isInstalled.next(true);

      this.snippetsSubject.next(evt["detail"]["snippets"]);
      this.workingSnippet.next(evt["detail"]["snippets"][evt["detail"]["working"]]);
      this.working_snippet = evt.detail.working;
    });

    window.addEventListener("onSnippetChange", (evt: CustomEvent) => {
      let payload = evt.detail;
      let value = this.snippetsSubject.getValue();

      if (payload.action == "save") {
        // SAVED SNIPPET NOT LONGER AVAILABLE
        // Close snippet on save
        value[payload.working] = null;
        this.snippetsSubject.next(value);
      } else if (payload.action == "create") {
        // CREATE DRAFT FROM SCRATCH
        value[payload.working] = payload.snippet;
        this.snippetsSubject.next(value);
      } else if (payload.action == "open") {
        // OPEN SNIPPET FROM THE WEB
        value[payload.working] = payload.snippet;
        this.snippetsSubject.next(value);
      } else if (payload.action == "edit-state") {
        console.log("TODO: ADD edit state handling");
      } else {
        alert("Unknow snippet action: " + payload.action);
      }
    });

    window.addEventListener("onSnippetItemChange", (evt: CustomEvent) => {
      let payload = evt.detail;
      this.handleItemChange(payload.action, payload);
    });

    setTimeout(() => {
      console.log("REQUEST INITIAL STATE!!!!");
      window.dispatchEvent(new CustomEvent("onSnipettorAction", {
        detail: {
          action: "GetInitialState"
        }
      }));

      setTimeout(() => {
        this.isInstalled.next(this.initialized ? true : false);
      }, 300);

    }, 200);

  }

  isPluginInstalled() {
    return this.isInstalled.asObservable();
  }

  private initsnippetSubject() {
    this.snippetsSubject = new BehaviorSubject<SnippetInfo[]>([]);
    this.isInstalled = new ReplaySubject<Boolean>(1);
    // for debug purpose only
    // this.isInstalled.next(false);
  }

  private handleItemChange(action, payload) {
    switch (action) {
      case 'add' : {
        this.onAddItem(payload);
      } break;
      case 'delete' :
      case 'remove' : {
        this.onRemoveItem(payload);
      } break;
      case 'update' : {
        this.onUpdateItem(payload);
      } break;
      case 'move' : {
        this.onMoveItem(payload);
      } break;
      default: {
        alert("Unknow snippet action: " + payload.action);
      }
    };
  }

  getWorkingSnippet() {
    return this.workingSnippet.asObservable();
  }

  listSnippets(): Observable<SnippetInfo[]> {
    return this.snippetsSubject.asObservable();
  }

  subscribeSnippet(snippet) {
    let snippets = this.snippetsSubject.getValue();
    let idx = snippets.indexOf(snippet);

    if (idx < 0) return;

    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "subscribe",
        payload: {
          payload: {
            uid: snippet.uid // TODO: not good for working snippet
          }
        }
      }
    }));

    this.workingSnippet.next(snippets[idx]);
  }

  openSnippet(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "open-snippet",
        data: {
          payload: payload
        }
      }
    }));
  }

  selectSnippetByIndex(index) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "select-snippet",
        data: index
      }
    }));
  }

  onSavedSnippet(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "saved-draft",
        payload: payload
      }
    }));
  }

  onEditCurrentSnippet(payload) {
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "edit-current-snippet",
        payload: payload
      }
    }));
  }

  updateSnippet(payload) {
    payload.working = this.working_snippet;
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "update-snippet",
        payload: payload
      }
    }));
  }

  updateSnippetItem(action, payload) {
    payload.working = this.working_snippet;
    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action:  `${action}-snippet-item`,
        payload: payload
      }
    }));
    this.handleItemChange(action, { payload: payload });
  }

  closeSnippet(payload) {
    // reset working snippet
    let value = this.snippetsSubject.getValue();
    if (payload == this.workingSnippet) {
      this.workingSnippet.next(null);
      this.working_snippet = null;
    }

    let idx = value.indexOf(payload);
    if (idx >= 0) {
      value[idx] = null;
      this.snippetsSubject.next(value);
    }

    window.dispatchEvent(new CustomEvent("onSnipettorAction", {
      detail: {
        action: "saved-draft",
        payload: {
          uid:payload.uid
        }
      }
    }));
  }

  onAddItem(payload) {
    // Update current cache (TBD: fo future when it will be not necessary to reload all page)
    let value = this.snippetsSubject.getValue();
    payload = _.has(payload, 'payload') ? payload.payload : payload;
    value[payload.working].items.splice(payload.index, 0, payload.item);
    // Notify only if we are subscribed on it
    if (payload.working == this.working_snippet) {
      // Handle UI part
      this.workingSnippet.next(value[this.working_snippet]);
    }

    this.snippetsSubject.next(value);
  }

  onRemoveItem(payload) {
    let value = this.snippetsSubject.getValue();
    payload = _.has(payload, 'payload') ? payload.payload : payload;
    value[payload.working].items.splice(payload.index, 1);
    // Handle UI part
    if (payload.working == this.working_snippet) {
      this.workingSnippet.next(value[this.working_snippet]);
    }

    this.snippetsSubject.next(value);
  }

  onUpdateItem(payload) {
    let value = this.snippetsSubject.getValue();
    payload = _.has(payload, 'payload') ? payload.payload : payload;
    value[payload.working].items[payload.payload.idx].comment = payload.payload.item.comment;
    // Handle UI part
    if (payload.working == this.working_snippet) {
      this.workingSnippet.next(value[this.working_snippet]);
    }

    this.snippetsSubject.next(value);
  }

  onMoveItem(payload) {
    let value = this.snippetsSubject.getValue();
    payload = _.has(payload, 'payload') ? payload.payload : payload;
    let item = value[payload.working].items[payload.payload.oldIndex];
    value[payload.working].items.splice(payload.payload.oldIndex, 1);
    value[payload.working].items.splice(payload.payload.newIndex, 0, item);
    // Handle UI part
    if (payload.working == this.working_snippet) {
      this.workingSnippet.next(value[this.working_snippet]);
    }

    this.snippetsSubject.next(value);
  }

}
