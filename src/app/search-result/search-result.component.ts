import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {FirebaseListObservable } from 'angularfire2/database';

import {SocialService} from "app/shared/social/social.service";
import {AuthService} from "app/shared/auth/auth.service";

import {SnippetInfo} from "app/shared/extension/snippet-info";
import {SnippetorService} from 'app/extension/snippetor/snippetor.service';
import { VirtualScrollComponent } from 'angular2-virtual-scroll';


@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  window = window;
  @ViewChild('scroll') snippetsCmp: VirtualScrollComponent;
  snippets: SnippetInfo[];
  request: any;
  page: number;
  hasmore: boolean;
  startAt = 0;
  loading = true;
  gotEndEvent = false;
  count = 0;

  constructor(
    private auth: AuthService,
    private social: SocialService,
    private extSrv: SnippetorService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.startAt = 0;
    this.hasmore = true;
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      let mode = data["mode"]
      if (mode == "dashboard") {
        this.route.paramMap.subscribe(pMap => {
          let uid = pMap.get("uid");
          if (!uid) {
            this.auth.currentUser().subscribe(user => {
              this.request = { user: user.uid };
              this.social.listSnippets(this.request).subscribe(snippets => {
                this.loading = false;
                this.snippets = snippets;
                this.hasmore = snippets.length > 0 && snippets.length % 10 == 0;
              });
            });
          }
          else {
            this.request = { user: uid };
            this.social.listSnippets(this.request).subscribe(snippets => {
              this.loading = false;
              this.snippets = snippets;
              this.hasmore = snippets.length > 0 && snippets.length % 10 == 0;
            });
          }

        });
      }
      else {
        this.request = {};
        this.social.listSnippets().subscribe(snippets => {
          this.loading = false;
          this.snippets = snippets;
          this.hasmore = snippets.length > 0 && snippets.length % 10 == 0;
        });
      }
    });
    this.page = 1;
    this.hasmore = true;
    this.loading = true;
  }

  onLoadMore() {
    if (!this.hasmore || this.snippets.length % 10 != 0) {
      this.hasmore = false;
      return;
    }

    this.loading = true;
    this.page++;
    this.request.page = this.page;
    this.social.listSnippets(this.request).subscribe(snippets => {
      this.hasmore = snippets.length > 0 && snippets.length % 10 == 0;
      if (snippets.length >= this.page*10) {
        this.snippets = snippets;
      }
      else {
        this.snippets = [...this.snippets, ...snippets];
      }

      this.loading = false;
    });
  }

  // force extension to open snippet
  playSnippet(snippet) {
    this.extSrv.openSnippet(snippet);
  }

  scrollSnippets($event) {
    if ($event.end == this.page * 10 && !this.loading) {
      this.count++;
      if (this.count == 2) {
        this.loading = true;
        setTimeout(() => {
          this.onLoadMore();
        }, 200);
      }
    }
    else {
      this.count = 0;
    }
  }
}
