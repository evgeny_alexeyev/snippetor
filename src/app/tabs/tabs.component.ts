import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {SnippetInfo} from "app/shared/extension/snippet-info";
import {SnippetorService} from 'app/extension/snippetor/snippetor.service';
import {AuthService} from "app/shared/auth/auth.service";


@Component({
  selector: 'navigation-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  snippets: Observable<SnippetInfo[]>;
  mode: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private extSrv: SnippetorService,
    private auth: AuthService
  ) {

  }

  ngOnInit() {
    this.snippets = this.extSrv.listSnippets();
    this.snippets.subscribe(snip => {
      this.snippets = this.extSrv.listSnippets();
    });
    this.route.data
      .subscribe(data => {
        this.mode = data["mode"];
      });

  }

  currentUser() {
    return this.auth.currentUser();
  }

  isActive(id) {
    if (id == this.mode)
      return ['active'];

    if (id == 'all' && (["search", "index"].indexOf(this.mode) >= 0 || !this.mode))
      return ['active'];

    if (id == "snippets" && ["edit", "view", "draft"].indexOf(this.mode) >= 0)
    return ['active'];

    return [];
  }

  getSnippetsCount(sps: SnippetInfo[]) {
    let res = 0;
    for (let t in sps)
      if (sps[t])
        res++;
    return res;
  }

  openSnippet(snippet) {
    // Subscribe for the concreate snippet
    this.extSrv.subscribeSnippet(snippet);
    // change route after some time
    setTimeout(() => {
      this.router.navigate(["/edit", "draft"]);
    }, 100);
  }

}
