'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// Keeps track of the length of the 'likes' child list in a separate property.
exports.countfollowwrite = functions.database.ref('/follow/{followid}').onWrite(event => {
  const fInfo = event.data.val();

  // Return nothing if
  // - status was not changed or
  // - previous status was null and new status is false
  if (!fInfo
    || (event.data.previous.exists() && event.data.previous.val().status == fInfo.status)
    || (!event.data.previous.exists() && fInfo.status == false))
    return;

  const counter = (fInfo.status ? 1 : -1); // increase or decrease counter
  const ud = event.data.ref.parent.parent.child('user_details');
  const followerDetail = ud.child(fInfo.follower);
  //const followingDetail = functions.database.ref('/user_details/'+ fInfo.following);

  // Return the promise from countRef.transaction() so our function
  // waits for this async event to complete before it exits.
  return followerDetail.transaction(current => {
    if (current) {
      current.following += counter;
      if (fInfo.following == fInfo.follower) {
        current.selfFF = fInfo.status;
        current.followers += counter;
      }
    }
    return current;
  }).then(() => {
    if (fInfo.following != fInfo.follower)
      return ud.child(fInfo.following).transaction(current => {
        if (current)
          current.followers += counter;
        return current;
      });
  });
});

// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// })
